# Docker-libreoffice

## Description
Run an headless libreoffice server to use for automated processing.

This container use the healthcheck command to ensure that is running correctly.

## Build
```bash
docker build -t negetem/libreoffice .
```

## Run
```bash
docker run -d -p 8997:8997 negetem/libreoffice
```

## Contributing
Please submit and comment on bug reports and feature requests.

To submit a patch:

1. Fork it (https://bitbucket.org/negetem/docker-libreoffice/fork).
2. Create your feature branch (*git checkout -b my-new-feature*).
3. Make changes.
4. Commit your changes (*git commit -am 'Add some feature'*).
5. Push to the branch (*git push origin my-new-feature*).
6. Create a new Pull Request.


## License
This app is licensed under the MIT license.

## Warranty
This software is provided "as is" and without any express or
implied warranties, including, without limitation, the implied
warranties of merchantibility and fitness for a particular
purpose.
